#!/usr/bin/env python
# -*- coding: utf-8 -*-

from odbAccess import openOdb
import os
import datetime
import numpy as np
import pickle
from run_Classes import Amplitude, Pattern, Job
from run_Classes import ElementList, Slice, WorkFlowError

user = os.environ["USER"]

div = ","
nl = "\n"
step_divider_str = "-" * 20 + nl
done_str = "...done"


def evaluate_rose(job, z_start_end=(1.0, 0.0), dz=5e-2,
                  region_name="RegionOfInterest", csys_name=None,
                  pattern_is_alternating=False,
                  write_sets_to_odb=True
                  ):
    """
        Evaluates stress and plastic strain wind rose in job's implicit
        simulation database for cubes with xy positions according to
        job.pattern's wind rose. Assembles data in ElementList instances and
        calls averaging scheme on them. Writes dictionary in pickle file.
        Allows for selection of element subset and change to predefined
        coordinate system for physical quantities.

        :param job: Job instance with Pattern
        :type job: Job
        :param z_start_end: max and min of evaluated probe depth; in [mm]
        :param dz: thickness of slice cubes; in [ mm]
        :param region_name: name of element set. Used to call getSubset() on
        fieldOutputs
        :type region_name: str
        :param csys_name: name of datum coordinate system. Used to call
        getTransformedField() on fieldOutputs
        :type csys_name: str
        :param pattern_is_alternating: Flag indicating if pattern has rows
        with alternating shift in y
        :type pattern_is_alternating: bool
        :param write_sets_to_odb: Boolean to toggle creation of ElementSets
        in .odb for identified slices
        :return: element_list_dict: dict of dict of ElementLists, keys are ["C",
        ..., "NW"][0, ..., len(z_center)]
        """
    print(step_divider_str + "Starting single evaluation...")

    if region_name is None:
        region_name = "all"
        evaluate_subset = False
    else:
        evaluate_subset = True
    region_name = region_name.upper()

    if csys_name is None:
        change_to_csys = False
    else:
        change_to_csys = True
    csys_name = "ASSEMBLY_PLATE-1_ORI-1"

    if not os.path.exists("elem_list_dict.pkl"):
        exec_time = datetime.datetime.now()
        odb = openOdb(job.name + "_impl" + ".odb",
                      # readOnly=True
                      )
        part = odb.rootAssembly.instances["PLATE-1"]
        elset = part.elementSets[region_name]
        no_elem = len(elset.elements)
        step_identifier = odb.steps.keys()[-1]
        fields_root = odb.steps[step_identifier].frames[-1]
        csys = odb.rootAssembly.datumCsyses[csys_name]

        # coords = fields_root.fieldOutputs["COORD"]
        coords = load_pickle_file("ROI_bulkDataBlocks_data.pkl")
        labels_from_raw = load_pickle_file(
            "ROI_bulkDataBlocks_elementLabels.pkl")
        pe = fields_root.fieldOutputs["PE"]
        s = fields_root.fieldOutputs["S"]
        evol = fields_root.fieldOutputs["EVOL"]

        if evaluate_subset:
            # coords = coords.getSubset(region=elset)
            pe = pe.getSubset(region=elset)
            s = s.getSubset(region=elset)
            evol = evol.getSubset(region=elset)

        if change_to_csys:
            pe = pe.getTransformedField(datumCsys=csys)
            s = s.getTransformedField(datumCsys=csys)

        # coords = coords.bulkDataBlocks[0]
        # coords = coords.data

        pe = pe.bulkDataBlocks[0]
        all_labels = pe.elementLabels
        labels_are_equal = (labels_from_raw == all_labels).all()
        if labels_are_equal:
            print("Labels from source raw equal those of job_impl - fine.")
        else:
            print("Labels mismatch! - breaking.")
            raise WorkFlowError

        pe = pe.data
        s = s.bulkDataBlocks[0].data
        evol = evol.bulkDataBlocks[0].data

        z_center = np.arange(z_start_end[0] - dz / 2, z_start_end[1], - dz)

        x = coords[:, 0]
        y = coords[:, 1]
        z = coords[:, 2]

        d = job.pattern.rose_coords_dict
        edge = job.pattern.rep_vol_edge
        mask_n = ((d["N"][0] - edge[0] / 2 <= x)
                  & (x < d["N"][0] + edge[0] / 2))

        if pattern_is_alternating:
            edge = np.multiply(np.array([2, 1]), job.pattern.rep_vol_edge)

        mask_cx = ((d["C"][0] - edge[0] / 2 <= x)
                   & (x < d["C"][0] + edge[0] / 2))
        mask_s = ((d["S"][0] - edge[0] / 2 <= x)
                  & (x < d["S"][0] + edge[0] / 2))
        mask_w = ((d["W"][1] - edge[1] / 2 <= y)
                  & (y < d["W"][1] + edge[1] / 2))
        mask_cy = ((d["C"][1] - edge[1] / 2 <= y)
                   & (y < d["C"][1] + edge[1] / 2))
        mask_e = ((d["E"][1] - edge[1] / 2 <= y)
                  & (y < d["E"][1] + edge[1] / 2))

        mask_dict = {
            "N": mask_n & mask_cy,
            "NE": mask_n & mask_e,
            "E": mask_cx & mask_e,
            "SE": mask_s & mask_e,
            "S": mask_s & mask_cy,
            "SW": mask_s & mask_w,
            "W": mask_cx & mask_w,
            "NW": mask_n & mask_w,
            "C": mask_cx & mask_cy
        }

        elem_list_dict = {}
        for key in Pattern.rose:
            elem_list_dict[key] = {}

        for z_idx in range(len(z_center)):
            mask_z = ((z_center[z_idx] - dz / 2 < z)
                      & (z <= z_center[z_idx] + dz / 2))
            for key in mask_dict.keys():
                xyz = list(job.pattern.rose_coords_dict[key]) + [z_center[z_idx]]
                # elem_list_dict[key][z_idx] = ElementList(xyz, dz,
                #                                          job.pattern.rep_vol_edge)
                _edge = edge
                if pattern_is_alternating and key[0] == "N":  # N, NE, NW
                    _edge = job.pattern.rep_vol_edge
                elem_list_dict[key][z_idx] = ElementList(xyz, dz, _edge)
                e = elem_list_dict[key][z_idx]

                mask = mask_z & mask_dict[key]
                labels = all_labels[mask]
                pstrain = pe[mask]
                stress = s[mask]
                ev = evol[mask]
                # print("Assembling ElementList " + key + "-" + str(z_idx)
                #       + " with " + str(labels.shape[0]) + " elements...")

                e.no_elements = labels.shape[0]
                e.labels_lst += list(labels)
                e.evol_lst += list(ev)
                for i in range(6):
                    e.stress_lst[i] += list(stress[:, i])
                    e.pstrain_lst[i] += list(pstrain[:, i])

                # print("...computing volume average ...")
                e.compute_volume_weighted_average()
                e.evol_lst, e.stress_lst, e.pstrain_lst = [], [], []

                if write_sets_to_odb:
                    set_str = key + "-" + str(z_idx)
                    new_str = "new"
                    while set_str in part.elementSets.keys():
                        set_str += new_str

                    part.ElementSetFromElementLabels(name=set_str,
                                                     elementLabels=tuple(
                                                         labels))

        odb.close()
        exec_time = datetime.datetime.now().replace(
            microsecond=0) - exec_time
        write_pickle_file(elem_list_dict, "elem_list_dict")
        write_pickle_file(exec_time, "eval_exec_time")
        write_rose(elem_list_dict, job)
    else:
        elem_list_dict = load_pickle_file("elem_list_dict.pkl")
        exec_time = load_pickle_file("eval_exec_time.pkl")

    job.runtime_dict["rose evaluation"] = exec_time
    return elem_list_dict


def create_slices(nrose_values):
    """
    Spawns Slice instances for wind rose patches and number of slices in z.
    Assigns key string and 6-valued plastic strains.
    :param nrose_values:
    :return: dictionary with <wind rose patch>-<z-idx> as keys,
    Slice instance as values
    """
    slice_dict = {}
    no_slices = len(nrose_values.keys()) * len(nrose_values["C"].keys())

    print(step_divider_str + "Spawning " + str(no_slices) + " Slice instances...")
    for key in nrose_values.keys():
        xy_rose = nrose_values[key]
        for idx in xy_rose.keys():
            key_str = key + "-" + str(idx)
            slice_dict[key_str] = Slice(key_str,
                                        xy_rose[idx].pstrain)

    print(done_str)
    return slice_dict

def assign_elements(target, _slice_dict, z_start_end=(1.0, 0.), dz=5e-2):
    """
    Iterates over target raw odb and writes assigned elements' labels to
    Slice instances in nslice_dict
    :param target: Pattern instance of target geometry
    :type target: Pattern
    :param _slice_dict: dictionary with <wind rose patch>-<z-idx> as keys,
    Slice instance as values
    :param z_start_end: max and min of evaluated probe depth; in [mm]
    :param dz: thickness of slice cubes; in [ mm]
    :return: _slice_dict
    """

    exec_time = datetime.datetime.now()
    target_raw_name = "target_raw.odb"
    odb = openOdb(target_raw_name)
    part = odb.rootAssembly.instances["PLATE-1"]
    region_name = "Peened_Volume".upper()
    elset = part.elementSets[region_name].elements
    z_center = np.arange(z_start_end[0] - dz / 2, z_start_end[1], - dz)

    step_identifier = odb.steps.keys()[-1]
    fields_root = odb.steps[step_identifier].frames[-1]
    coords = fields_root.fieldOutputs["COORD"]
    coords = coords.bulkDataBlocks[0]
    x = coords.data[:, 0]
    y = coords.data[:, 1]
    z = coords.data[:, 2]

    d = target.rose_coords_dict
    if target.is_alternating:
        north_edge = np.multiply(np.array([1, 1]), target.rep_vol_edge)
        edge = np.multiply(np.array([2, 1]), target.rep_vol_edge)

        mask_n = ((d["N"][0] - north_edge[0] / 2 <= x)
                  & (x < d["N"][0] + north_edge[0] / 2))
        mask_cx = ((d["N"][0] + north_edge[0] / 2 <= x)
                   & (x < d["S"][0] - edge[0] / 2))
    else:
        edge = target.rep_vol_edge
        mask_n = ((d["N"][0] - edge[0] / 2 <= x)
                  & (x < d["N"][0] + edge[0] / 2))
        mask_cx = ((d["N"][0] + edge[0] / 2 <= x)
                   & (x < d["S"][0] - edge[0] / 2))

    mask_s = ((d["S"][0] - edge[0]/2 <= x) & (x < d["S"][0] + edge[0]/2))
    mask_w = ((d["W"][1] - edge[1]/2 <= y) & (y < d["W"][1] + edge[1]/2))
    mask_cy = ((d["W"][1] + edge[1]/2 <= y) & (y < d["E"][1] - edge[1]/2))
    mask_e = ((d["E"][1] - edge[1]/2 <= y) & (y < d["E"][1] + edge[1]/2))

    mask_dict = {
        "N": mask_n & mask_cy,
        "NE": mask_n & mask_e,
        "E": mask_cx & mask_e,
        "SE": mask_s & mask_e,
        "S": mask_s & mask_cy,
        "SW": mask_s & mask_w,
        "W": mask_cx & mask_w,
        "NW": mask_n & mask_w,
        "C": mask_cx & mask_cy
    }
    print("Assigning " + str(len(elset)) + " elements in target region=" + region_name + "...")
    for z_idx in range(len(z_center)):
        mask_z = ((z_center[z_idx] - dz / 2 < z)
                  & (z <= z_center[z_idx] + dz / 2))
        for key in mask_dict.keys():
            mask = mask_z & mask_dict[key]
            key_str = key + "-" + str(z_idx)
            labels = coords.elementLabels[mask]
            # print(key_str + ":\t" + str(labels.shape[0]) + " elements.")
            _slice_dict[key_str].label_lst += list(labels)
    print(done_str)
    odb.close()
    exec_time = datetime.datetime.now() - exec_time

    return _slice_dict, exec_time


def write_slices(nslice_dict):
    """
    For all slices, writes ELSETs in "target_el_set.inp" and anisotropic
    expansion entries
    in "target_mat.inp".
    :param nslice_dict: dictionary, Slice instances as values
    :return:
    """
    print(step_divider_str + "Writing slices' data to .inp files...")
    el_set_str, mat_str, section_str = "", "", ""

    for a_slice in nslice_dict.values():
        el_set_str += a_slice.write_el_set()
        section_str += a_slice.write_section()
        mat_str += a_slice.write_material_entry()

    section_str += "\n*End Part"
    with open("target_el_set.inp", "w") as el_set_file:
        el_set_file.write(el_set_str)
        el_set_file.write(section_str)

    with open("target_mat.inp", "w") as mat_file:
        mat_file.write(mat_str)

    print(done_str)


def create_target_input():
    """
    Writes target.inp and includes mesh.inp, target_el_set.inp,
    target_assembly.inp, target_mat.inp and target_steps.inp.
    :return:
    """
    print(step_divider_str + "Creating target input...")
    file_name = "target.inp"
    preamble = "*INCLUDE, INPUT="

    files_target = [
        "target_mesh.inp",
        "target_el_set.inp",
        "target_assembly.inp",
        "target_mat.inp",
        "target_steps.inp",
    ]

    with open(file_name, "w") as file:
        for f in files_target:
            file.write(preamble + f + nl)

    print(done_str)
